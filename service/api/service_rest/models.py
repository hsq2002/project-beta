from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)


class Technician(models.Model):
    name = models.CharField(max_length=17, unique=True)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_show_tecnician", kwargs={"id": self.id})


class Service(models.Model):
    VIN = models.CharField(max_length=17, unique=True)
    owner_name = models.CharField(max_length=200)
    appointment_time = models.DateTimeField(null=True)
    reason = models.CharField(max_length=300)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )
    finished = models.BooleanField(default=False)




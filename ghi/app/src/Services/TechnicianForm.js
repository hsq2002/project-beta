import React, { useState, useEffect } from "react";

function TechnicianForm(props) {
    const[technician, setTechnician] = useState([]);

    const[name, setName] = useState(" ");
    const handlenameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const[employee_number, setNumber] = useState(" ");
    const handleNumberChange = (event) => {
        const value = event.target.value;
        setNumber(value)
    }
    const fetchData = async () => {
    const url = "http://localhost:8080/api/technicians/";

    const response = await fetch (url);


        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technicians)
        }
    }
        useEffect(() => {
            fetchData();
        }, []);

        const handleSubmit = async (event) => {
            event.preventDefault();
            const data = {};
            data.name = name;
            data.employee_number = employee_number;


        const technicianUrl = "http://localhost:8080/api/technicians/";

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-type": "application/json",
            },
        };
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            console.log(newTechnician);
        };
            setName(" ");
            setNumber(" ");
        }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Enter a Technician</h1>
              <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input value={name} onChange={handlenameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Employee Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={employee_number} onChange={handleNumberChange} placeholder="number" required type="number" name="number" id="number" className="form-control" />
                  <label htmlFor="room_count">Employee Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );

                  }
export default TechnicianForm;

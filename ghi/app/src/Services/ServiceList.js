import React, { useState, useEffect } from "react";


const ServiceList = () => {
    const[services, setServicesList] = useState([]);
    const fetchData = async () => {
      const url = "http://localhost:8080/api/services/";

      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setServicesList(data.services)
        }
      };

      useEffect(() => {
        fetchData();
      }, []);

      const handleDelete = async (id) => {
        const servicelisturl = "http://localhost:8080/api/services/${id}/";
        const fetchConfig = {
          method: "delete",
          headers: {
            "Content-Type": "application/json",
          },
        };

        const response = await fetch(servicelisturl, fetchConfig);

        if (response.ok) {
          fetchData();
        } else {
          alert("services was not detected!");
        }
      };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer Name</th>
                    <th>Appointment Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                </tr>
            </thead>
            <tbody>
                {services.map(appointment =>{
                  return(
                    <tr key={ appointment.VIN }>
                        <td>{ appointment.customer_name }</td>
                        <td>{ appointment.appointment_time }</td>
                        <td>{ appointment.technician }</td>
                        <td>{ appointment.reason }</td>
                    </tr>
                  );
                })}
            </tbody>
        </table>
    )
            }
export default ServiceList;

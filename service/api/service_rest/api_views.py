from .models import Service, AutomobileVO, Technician
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from django.http import JsonResponse
from .encoders import ServiceDetailEncoder, ServiceListEncoder, TechnicianEncoder


@require_http_methods(["GET", "POST"])
def api_list_services(request):
    if request.method == "GET":
        services = Service.objects.all()
        return JsonResponse(
            {"services": services},
            encoder=ServiceListEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            services = Service.objects.create(**content)
            return JsonResponse(
                services,
                encoder=ServiceDetailEncoder,
                safe=False
            )
        except Exception as e:
            return JsonResponse(str(e), safe=False)


@require_http_methods(["DELETE", "GET"])
def api_show_services(request, pk):
    if request.method == "GET":
        try:
            services = Service.objects.get(id=pk)
            return JsonResponse(
                services,
                encoder=ServiceDetailEncoder,
                safe=False
            )
        except BaseException:
            return JsonResponse(
                {"message": f"service {pk} does not exist"}
            )
    else:
        try:
            count, _ = Service.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except BaseException:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            services = Technician.objects.create(**content)
            return JsonResponse(
                services,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Exception as e:
            return JsonResponse(str(e), safe=False)


@require_http_methods(["DELETE", "GET"])
def api_show_technician(request, pk):
    if request.method == "GET":
        try:
            technicians = Technician.objects.get(id=pk)
            return JsonResponse(
                technicians,
                encoder=TechnicianEncoder,
                safe=False
            )
        except BaseException:
            return JsonResponse(
                {"message": f"service {pk} does not exist"}
            )
    else:
        try:
            count, _ = Technician.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except BaseException:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def service_history_list(request):
    if request.method == "GET":
        services = Service.objects.all()
        return JsonResponse(
            {"services": services},
            encoder=ServiceListEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            services = Service.objects.create(**content)
            return JsonResponse(
                services,
                encoder=ServiceDetailEncoder,
                safe=False
            )
        except Exception as e:
            return JsonResponse(str(e), safe=False)

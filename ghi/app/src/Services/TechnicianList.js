import { useState, useEffect } from "react";

const TechnicianList = () => {
  const [technicians, setTechnician] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
      const data = await response.json();
      setTechnician(data.technicians);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (event) => {
    const url = `http://localhost:8080/api/technicians/${event.target.id}`;

    const fetchConfigs = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfigs);
    const data = await response.json();

    setTechnician(
      technicians.filter(
        (technician) => String(technician.id) !== event.target.id
      )
    );
  };
  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Technicians</h1>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>employee number</th>
                </tr>
              </thead>

              <tbody>
                {technicians.map((technician) => {
                  return (
                    <tr key={technician.id}>
                      <td>{technician.name}</td>
                      <td>{technician.employee_number}</td>
                      <td>
                        <button
                          onClick={handleDelete}
                          id={technician.id}
                          className="btn btn-danger"
                        >
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default TechnicianList;

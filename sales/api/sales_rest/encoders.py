from .models import AutomobileVO, SalesPerson, Customer, Sales
from common.json import ModelEncoder


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['vin']


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ['sales_person_name', 'employee_number', 'id']


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ['customer_name', 'address', 'phone_number', 'id']


class SalesListEncoder(ModelEncoder):
    model = Sales
    properties = ['vin', 'sales_person', 'customer', 'sales_price', "pk"]
    encoders = {
        'vin': AutomobileVOEncoder(),
        'sales_person': SalesPersonEncoder(),
        'customer': CustomerEncoder(),

    }

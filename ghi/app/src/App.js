import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerForm from "./sales/CustomerForm";
import SalesPersonForm from "./sales/SalesPersonForm";
import SalesRecordForm from "./sales/SalesRecordForm";
import SalesRecordList from "./sales/SalesRecordList";
import ServiceList from "./Services/ServiceList";
import TechnicianForm from "./Services/TechnicianForm";
import ServiceHistory from "./Services/ServiceHistory";
import ModelForm from "./inventory/VehicleModel";
import Manufacturer from "./inventory/Manufacturer";
import ManufactureList from "./inventory/ManufactureList";
import ManufacturerForm from "./inventory/Manufacturer";
import ServiceAppointment from "./Services/CreateServiceAppointment";
import AutomobileForm from "./inventory/AutomobileForm";
import VehicleList from "./inventory/VehicleList";
import AutomobileList from './inventory/AutomobileList';
import TechnicianList from "./Services/TechnicianList";



function App(props) {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians" element={<TechnicianForm />} />
          <Route path="/technicianlist" element={<TechnicianList />} />
          <Route path="/history" element={<ServiceHistory />} />
          <Route path="/vehicles/new" element={<ModelForm />} />
          <Route path="/manufacturers" element={<Manufacturer />} />
          <Route path="/manufacturers" element={<Manufacturer />} />       
          <Route path="/sales" element={<SalesRecordList sales={props.sales} />} />
          <Route path="/sales/new" element={<SalesRecordForm />} />
          <Route path="/salesperson/new" element={<SalesPersonForm/>}/>
          <Route path="/customer/new" element={<CustomerForm/>}/>
          <Route path="/manufacturers" element={<ManufactureList />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/appointments" element={<ServiceAppointment />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/vehicles" element={<VehicleList />} />
          <Route path="/manufacturerlist" element={<ManufactureList />} />
          <Route path="/appointments" element={<ServiceAppointment />} />
          <Route path="/automobiles" element={<AutomobileForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;

from django.urls import path
from .views import api_list_sales, api_show_sale, api_list_customers, api_show_customer, api_list_salespeople, api_show_salesperson

urlpatterns = [
    path('sales/', api_list_sales, name='api_list_sales'),
    path('sales/<int:pk>/', api_show_sale, name='api_show_sales'),
    path('customer/', api_list_customers, name='api_list_customers'),
    path('customer/<int:id>/', api_show_customer, name='api_show_customer'),
    path('salespeople/', api_list_salespeople, name='api_list_salespeople'),
    path(
        'salespeople/<int:id>/',
        api_show_salesperson,
        name='api_show_salesperson')
]

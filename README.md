# CarCar

Team:

* Humza Qureshi - Services
* Kane Rodriguez - Sales

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.
The models for the inventory microservice were the following: manufacturers, vahicles, and automobiles. The three models fulfilled the role for integrating the inventory microservice, through our APIs. After creating the with our javascript frontend, we would then be presented with our lists. We gain access to these lists by fetching from our url, which is a resourceful component of our insomnia requests.

## Sales microservice

We have several models in our system, each with its own way of integrating with the inventory microservice. The AutomobileVO model only has one property, the VIN, which is all we need to identify the unique car. The SalesPerson model has two properties, the salesperson's name and their employee number, which is used to identify them uniquely.

The Customer model has three properties: the customer's name, address, and phone number. We made the address and phone number unique because we assumed that people with the same name could live in different places and have different phone numbers.

Finally, the Sales model has four properties: VIN, salesperson, customer, and sales price. The first three properties are foreign keys from the previous models. We also added a method to get the API URL, which returns the URL for the api_show_sale view. We use the ID value to get the sale object and return the URL.

To integrate with the inventory microservice, we use polling to retrieve data from the AutomobileVO model. This allows us to get the VIN number, which is necessary, along with the previously mentioned models, to create new sales or list previous sales.